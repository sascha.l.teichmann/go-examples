package main

import (
	"fmt"
	"os"
)

func hello(s string) {
	fmt.Printf("Hello, %s!\n", s)
}

func main() {
	for _, s := range os.Args[1:] {
		hello(s)
	}
}
