package main

import (
	"flag"
	"image"
	"image/color"
	"image/png"
	"log"
	"os"
	"runtime"
	"sync"
)

var palette = color.Palette{
	color.RGBA{R: 0, G: 0, B: 0, A: 255},       // black
	color.RGBA{R: 66, G: 30, B: 15, A: 255},    // brown 3
	color.RGBA{R: 25, G: 7, B: 26, A: 255},     // dark violett
	color.RGBA{R: 9, G: 1, B: 47, A: 255},      // darkest blue
	color.RGBA{R: 4, G: 4, B: 73, A: 255},      // blue 5
	color.RGBA{R: 0, G: 7, B: 100, A: 255},     // blue 4
	color.RGBA{R: 12, G: 44, B: 138, A: 255},   // blue 3
	color.RGBA{R: 24, G: 82, B: 177, A: 255},   // blue 2
	color.RGBA{R: 57, G: 125, B: 209, A: 255},  // blue 1
	color.RGBA{R: 134, G: 181, B: 229, A: 255}, // blue 0
	color.RGBA{R: 211, G: 236, B: 248, A: 255}, // lightest blue
	color.RGBA{R: 241, G: 233, B: 191, A: 255}, // lightest yellow
	color.RGBA{R: 248, G: 201, B: 95, A: 255},  // light yellow
	color.RGBA{R: 255, G: 170, B: 0, A: 255},   // dirty yellow
	color.RGBA{R: 204, G: 128, B: 0, A: 255},   // brown 0
	color.RGBA{R: 153, G: 87, B: 0, A: 255},    // brown 1
	color.RGBA{R: 106, G: 52, B: 3, A: 255},    // brown 2
}

func iterate(p complex128, max int) int {
	z := p
	for i := 0; i < max; i++ {
		if re, im := real(z), imag(z); re*re+im*im > 4 {
			return i
		}
		z = z*z + p
	}
	return max
}

func mandelbrot(pic *image.Paletted, from, to complex128, max, workers int) {

	w, h := pic.Rect.Dx(), pic.Rect.Dy()

	dx := complex(real(to-from)/float64(w), 0)
	dy := complex(0, imag(to-from)/float64(h))

	type job struct {
		p   complex128
		row []uint8
	}

	jobs := make(chan job)

	var wg sync.WaitGroup

	for i := 0; i < workers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			mod := len(pic.Palette) - 1
			for j := range jobs {
				p, row := j.p, j.row
				for i := range row {
					if k := iterate(p, max); k < max {
						row[i] = uint8(k%mod) + 1
					}
					p += dx
				}
			}
		}()
	}

	for pix := pic.Pix; len(pix) > 0; from, pix = from+dy, pix[w:] {
		jobs <- job{p: from, row: pix[:w]}
	}
	close(jobs)
	wg.Wait()
}

func positive(x int) int {
	if x < 1 {
		return 1
	}
	return x
}

func main() {
	var (
		iterations        int
		workers           int
		imWidth, imHeight int
		x, y              float64
		width, height     float64
		output            string
	)

	flag.IntVar(&iterations, "iterations", 1000, "number of max iterations")
	flag.IntVar(&workers, "worker", runtime.NumCPU(), "number of workers")
	flag.IntVar(&imWidth, "imagewidth", 1024, "width of image")
	flag.IntVar(&imHeight, "imageheight", 1024, "height of image")
	flag.Float64Var(&x, "x", -2, "x corner")
	flag.Float64Var(&y, "y", -1.2, "y corner")
	flag.Float64Var(&width, "width", 2.5, "width")
	flag.Float64Var(&height, "height", 2.5, "height")
	flag.StringVar(&output, "output", "out.png", "output filename")

	flag.Parse()

	r := image.Rect(0, 0, positive(imWidth), positive(imHeight))

	pic := image.NewPaletted(r, palette)

	var (
		from = complex(x, y)
		to   = complex(x+width, y+height)
	)

	mandelbrot(pic, from, to, iterations, positive(workers))

	f, err := os.Create(output)
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}

	if png.Encode(f, pic); err != nil {
		f.Close()
		log.Fatalf("error: %v\n", err)
	}

	if err := f.Close(); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
