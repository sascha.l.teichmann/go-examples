package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"strings"
)

type field byte

const (
	empty field = iota
	cross
	circle
)

/*
  0 1 2
  3 4 5
  6 7 8
*/

type board [3 * 3]field

var checks = [][]byte{
	0: {0, 1, 2},
	1: {3, 4, 5},
	2: {6, 7, 8},
	3: {0, 3, 6},
	4: {1, 4, 7},
	5: {2, 5, 8},
	6: {0, 4, 8},
	7: {2, 4, 6},
}

var indexToCheck = [][]byte{
	0: {0, 3, 6},
	1: {0, 4},
	2: {0, 5, 7},
	3: {1, 3},
	4: {1, 4, 6, 7},
	5: {1, 5},
	6: {2, 3, 7},
	7: {2, 4},
	8: {2, 5, 6},
}

func (f field) other() field {
	switch f {
	case cross:
		return circle
	case circle:
		return cross
	default:
		return empty
	}
}

func (f field) String() string {
	switch f {
	case cross:
		return "X"
	case circle:
		return "O"
	default:
		return " "
	}
}

func (b *board) win(idx int) field {
	for _, ci := range indexToCheck[idx] {
		s := checks[ci]
		if v := b[s[2]]; v != empty && v == b[s[0]] && v == b[s[1]] {
			return v
		}
	}
	return empty
}

func (b *board) rate(idx int, who field) int {
	b[idx] = who

	var r int
	switch w := b.win(idx); {
	case w == who:
		// a direct winning move
		r = math.MaxInt32
	case w == empty:
		// count wins and loses
		var counting func(field)
		counting = func(ply field) {
			for i, v := range b {
				if v == empty {
					b[i] = ply
					if b.win(i) == ply {
						if ply == who {
							r++
						} else {
							r--
						}
					} else {
						counting(ply.other())
					}
					b[i] = empty
				}
			}
		}
		counting(who.other())
	default:
		panic("This should not happen!")
	}
	b[idx] = empty
	return r
}

func (b *board) randomMove(who field) {
	for {
		if idx := rand.Intn(len(b)); b[idx] == empty {
			b[idx] = who
			break
		}
	}
}

func (b *board) findBest(who field) int {
	best, idx := 0, -1
	for i, v := range b {
		if v == empty {
			if r := b.rate(i, who); idx == -1 || r > best {
				best = r
				idx = i
			}
		}
	}
	return idx
}

func (b *board) String() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "1│%s│%s│%s\n", b[0], b[1], b[2])
	fmt.Fprintln(&buf, "─┼─┼─┼─")
	fmt.Fprintf(&buf, "2│%s│%s│%s\n", b[3], b[4], b[5])
	fmt.Fprintln(&buf, "─┼─┼─┼─")
	fmt.Fprintf(&buf, "3│%s│%s│%s\n", b[6], b[7], b[8])
	fmt.Fprintln(&buf, "─┼─┼─┼─")
	fmt.Fprintln(&buf, " │a│b│c")
	return buf.String()
}

func parseField(line string) (int, bool) {
	find := func(rs []rune) int {
		m := -1
		for i, r := range rs {
			if strings.IndexRune(line, r) >= 0 {
				m = i
			}
		}
		return m
	}
	maxRow := find([]rune{'1', '2', '3'})
	maxCol := find([]rune{'a', 'b', 'c'})
	if maxRow == -1 || maxCol == -1 {
		return 0, false
	}
	return 3*maxRow + maxCol, true
}

func (b *board) isEmpty(idx int) bool {
	return idx < len(b) && b[idx] == empty
}

func (b *board) hasEmpty() bool {
	for _, v := range b {
		if v == empty {
			return true
		}
	}
	return false
}

func (b *board) clear() {
	for i := range b {
		b[i] = empty
	}
}

func main() {
	var b board
	var player field
	var beforeGame bool = true

	restart := func(format string, a ...interface{}) {
		fmt.Println()
		fmt.Println(&b)
		fmt.Printf(format, a...)
		beforeGame = true
		b.clear()
	}

	input := bufio.NewScanner(os.Stdin)
loop:
	for {
		fmt.Println()
		fmt.Println(&b)
		if beforeGame {
			fmt.Printf("Want to play %s or %s or quit ('x', 'o', 'q')?\n",
				cross, circle)
		} else {
			fmt.Println("Choose your field (1a - 3c)!")
		}
	again:
		if !input.Scan() {
			break
		}
		line := strings.ToLower(strings.TrimSpace(input.Text()))
		if beforeGame {
			switch line {
			case "q":
				break loop
			case "x":
				player = cross
			case "o":
				player = circle
			default:
				fmt.Println("Invalid input. Try again!")
				goto again
			}

			if player == circle {
				b.randomMove(cross)
			}

			beforeGame = false
			continue
		}
		idx, ok := parseField(line)
		if !ok {
			fmt.Println("Invalid input. Try again!")
			goto again
		}
		if !b.isEmpty(idx) {
			fmt.Println("Field already taken. Pick another")
			goto again
		}
		b[idx] = player
		if b.win(idx) == player {
			restart("%s win!\n", player)
			continue
		}

		best := b.findBest(player.other())
		if best == -1 {
			restart("Tie!\n")
			continue
		}

		b[best] = player.other()
		if b.win(best) == player.other() {
			restart("%s wins!\n", player.other())
			continue
		}

		if !b.hasEmpty() {
			restart("Tie!\n")
		}
	}

	if err := input.Err(); err != nil {
		log.Fatalf("Error: %v\n", err)
	}
}
