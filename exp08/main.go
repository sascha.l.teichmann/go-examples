package main

import (
	"io"
	"os"
	"strings"
)

type rot13Reader struct {
	io.Reader
}

func (rot *rot13Reader) Read(p []byte) (n int, err error) {
	n, err = rot.Reader.Read(p)
	for i, v := range p[:n] {
		switch {
		case v >= 'A' && v < 'N' || v >= 'a' && v < 'n':
			p[i] += 13
		case v > 'M' && v <= 'Z' || v > 'm' && v <= 'z':
			p[i] -= 13
		}
	}
	return
}

func main() {
	s := strings.NewReader(
		"Lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
