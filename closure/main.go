package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

func fib(n uint64) func() (uint64, bool) {
	i1, i2 := uint64(0), uint64(1)
	return func() (uint64, bool) {
		if n == 0 {
			return 0, false
		}
		n--
		x := i2
		i1, i2 = i2, i1+i2
		return x, true
	}
}

func main() {
	n := uint64(10)

	if len(os.Args) > 1 {
		var err error
		if n, err = strconv.ParseUint(os.Args[1], 10, 64); err != nil {
			log.Fatalf("not an integer: %v\n", err)
		}
	}

	f := fib(n)
	for v, ok := f(); ok; v, ok = f() {
		fmt.Printf("%d\n", v)
	}
}
