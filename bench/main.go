package main

import (
	"bytes"
	"fmt"
)

func repeat2(s string, n int) string {
	var buf bytes.Buffer
	for i := 0; i < n; i++ {
		buf.WriteString(s)
	}
	return buf.String()
}

func repeat1(s string, n int) string {
	var x string
	for i := 0; i < n; i++ {
		x += s
	}
	return x
}

func main() {
	//s := strings.Repeat("Hello, World\n", 10)
	s := repeat1("Hello, World\n", 10)
	fmt.Println(s)
}
