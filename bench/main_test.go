package main

import (
	"strings"
	"testing"
)

func BenchmarkRepeat1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = repeat1("Hello, World", 1000)
	}
}

func BenchmarkRepeat2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = repeat2("Hello, World", 1000)
	}
}

func BenchmarkRepeat3(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = strings.Repeat("Hello, World", 1000)
	}
}
