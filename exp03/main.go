package main

import (
	"fmt"
	"os"
)

func hello(s string) {
	fmt.Printf("Hello, %s!\n", s)
}

func main() {
	for i := 1; i < len(os.Args); i++ {
		hello(os.Args[i])
	}
}
