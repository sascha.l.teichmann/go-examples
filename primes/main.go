package main

import (
	"flag"
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
	"time"
)

func isPrime(n uint64) bool {
	if n <= 3 {
		return n > 1
	}
	if n%2 == 0 || n%3 == 0 {
		return false
	}
	for i := uint64(5); i*i <= n; i += 6 {
		if n%i == 0 || n%(i+2) == 0 {
			return false
		}
	}
	return true
}

func main() {

	var (
		workers = flag.Int("workers", runtime.NumCPU(), "Number workers")
		max     = flag.Uint64("max", 1_000_000, "Max number to test")
	)
	flag.Parse()

	start := time.Now()

	var total uint64

	if *max >= 2 {
		total = 1
	}

	chunks := make(chan [2]uint64)

	var wg sync.WaitGroup

	for i := 0; i < *workers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			var count uint64

			for chunk := range chunks {
				for candidate := chunk[0]; candidate <= chunk[1]; candidate += 2 {
					if isPrime(candidate) {
						count++
					}
				}
			}
			atomic.AddUint64(&total, count)
		}()
	}

	chunkSize := *max / uint64(*workers)

	const maxChankSize = 1 << 16

	switch {
	case chunkSize < 1:
		chunkSize = 2
	case chunkSize > maxChankSize:
		chunkSize = maxChankSize
	case chunkSize%2 != 0:
		chunkSize++
	}

	for pos := uint64(3); pos <= *max; pos += chunkSize {
		end := pos + chunkSize - 1
		if end > *max {
			end = *max
		}
		chunks <- [2]uint64{pos, end}
	}
	close(chunks)

	wg.Wait()

	dur := time.Since(start)

	fmt.Printf("Chunk size: %d\n", chunkSize)
	fmt.Printf("Find all primes up to: %d\n", *max)
	fmt.Printf("Time elasped: %.2f seconds\n", dur.Seconds())
	fmt.Printf("Number of primes found: %d\n", total)
}
