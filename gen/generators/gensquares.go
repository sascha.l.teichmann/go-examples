package main

import (
	"flag"
	"log"
	"os"
	"text/template"
)

var tmplTxt = `package main

// THIS IS A GENERATED FILE!
// BE CAREFUL WITH EDITING BY HAND.

var squares = []int{
	{{- range $s := . }}
	{{ $s }},
	{{- end }}
}
`
var tmpl = template.Must(template.New("squares").Parse(tmplTxt))

func makeSquares(n int) []int {
	if n < 0 {
		n = 0
	}
	squares := make([]int, n+1)
	for i := 0; i <= n; i++ {
		squares[i] = i * i
	}
	return squares
}

func main() {
	n := flag.Int("n", 10, "generate squares frim 0 to n")
	o := flag.String("o", "squares.go", "file to write squares to.")
	flag.Parse()
	f, err := os.Create(*o)
	if err != nil {
		log.Fatal(err)
	}

	err = tmpl.Execute(f, makeSquares(*n))
	f.Close() // TODO: extra error handling.
	if err != nil {
		log.Fatal(err)
	}
}
