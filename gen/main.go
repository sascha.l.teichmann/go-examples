package main

import "fmt"

func main() {
	for i, s := range squares {
		fmt.Printf("%2d: %3d\n", i, s)
	}
}
