package main

import (
	"fmt"
	"sync"
)

var names = [...]string{
	"Marx",
	"Aristoteles",
	"Spinoza",
	"Kant",
	"Russel",
}

const meals = 30

func dine(name string, left, right *sync.Mutex, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < meals; i++ {
		left.Lock()
		right.Lock()
		fmt.Printf("%s eats\n", name)
		right.Unlock()
		left.Unlock()
	}
}

func main() {

	var wg sync.WaitGroup

	forks := make([]sync.Mutex, len(names))

	for i, name := range names {
		wg.Add(1)
		go dine(name, &forks[i], &forks[(i+1)%len(forks)], &wg)
	}

	wg.Wait()
}
