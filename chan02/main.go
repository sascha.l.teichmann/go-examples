package main

import (
	"fmt"
	"time"
)

func f(c <-chan int) {
	time.Sleep(3 * time.Second)
	<-c
	<-c
	<-c
}

func main() {

	c := make(chan int, 2)
	go f(c)
	c <- 41
	fmt.Println("41 does not block")
	c <- 42
	fmt.Println("42 does not block")
	c <- 43
	fmt.Println("43 does block")
}
