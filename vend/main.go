package main

import (
	"fmt"

	"github.com/bradfitz/iter"
)

func main() {

	for i := range iter.N(10) {
		fmt.Println(i)
	}
}
