package iter

import "log"

func N(n int) []struct{} {
	log.Printf("Called own iter.N with n = %d\n", n)
	return make([]struct{}, n)
}
