package main

import "fmt"

func f(c <-chan int) {
	v := <-c
	fmt.Println(v)
}

func main() {

	c := make(chan int)

	go f(c)

	c <- 42
}
