package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
)

func fib(n uint64, out chan<- uint64) {
	defer close(out)
	i1, i2 := uint64(0), uint64(1)
	for ; n > 0; n-- {
		x := i1 + i2
		i1, i2 = i2, x
		out <- x
	}

}

func main() {
	n := uint64(10)

	if len(os.Args) > 1 {
		var err error
		if n, err = strconv.ParseUint(os.Args[1], 10, 64); err != nil {
			log.Fatalf("not an integer: %v\n", err)
		}
	}

	ch := make(chan uint64)
	go fib(n, ch)

	for v := range ch {
		fmt.Printf("%d\n", v)
	}
}
