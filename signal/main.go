package main

import (
	"fmt"
	"os"
	"os/signal"
	"time"
)

func main() {

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt, os.Kill)

	defer fmt.Println("Hallo")
	go func() {
		for i := 0; ; i++ {
			fmt.Printf("Zzzzz %d\n", i)
			time.Sleep(time.Second / 2)
		}
	}()

	<-sigChan
}
