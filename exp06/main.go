package main

import "fmt"

func main() {

	var x int
	var y int = 21
	z := 42

	var int int

	int = 84

	var (
		u = 10.0
		s = "This is a string!"
		c = 3 + 4i
	)

	fmt.Printf("x: %d\n", x)
	fmt.Printf("y: %d\n", y)
	fmt.Printf("z: %d\n", z)
	fmt.Printf("u: %f\n", u)
	fmt.Printf("s: %s\n", s)
	fmt.Printf("c: %f\n", c)
	fmt.Printf("int: %d\n", int)
}
