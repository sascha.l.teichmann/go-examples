module gitlab.com/sascha.l.teichmann/go-examples

go 1.24.0

require (
	github.com/bradfitz/iter v0.0.0-20191230175014-e8f45d346db8
	github.com/gorilla/mux v1.8.1
)
