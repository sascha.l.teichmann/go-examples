package main

import (
	"fmt"
	"os"
)

func hello(s string) {
	fmt.Printf("Hello, %s!\n", s)
}

func main() {
	for i := range os.Args[1:] {
		hello(os.Args[i])
	}
}
