package main

import (
	"fmt"
	"time"
)

func answers(out chan<- int) {
	for {
		out <- 42
	}
}

func run() {
	ch := make(chan int)
	go answers(ch)

	for answer := range ch {
		if answer == 42 {
			fmt.Println("Answer found!")
			return
		}
	}
}

func main() {
	run()
	time.Sleep(1 * time.Second)
}
