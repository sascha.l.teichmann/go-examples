package main

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io"
)

func generateKey() string {

	var b [512]byte
	if _, err := io.ReadFull(rand.Reader, b[:]); err != nil {
		panic("not enough entropy today")
	}
	s := sha1.Sum(b[:])
	return base64.URLEncoding.EncodeToString(s[:])
}

func main() {
	key := generateKey()
	fmt.Println(key)
}
