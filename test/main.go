package main

import "fmt"

func halfAnswer() int {
	return 20
}

func answer() int {
	return halfAnswer() + halfAnswer() + halfAnswer()
}

func whichAnswer(half bool) int {
	if half {
		return halfAnswer()
	}
	return answer()
}

func main() {
	fmt.Printf("The answer is: %d\n", answer())
}
