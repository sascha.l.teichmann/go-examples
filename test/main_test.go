package main

import "testing"

func TestAnswer(t *testing.T) {
	a := answer()
	if a != 42 {
		t.Errorf("answer: got %d wanted %d\n", a, 42)
	}
}

func TestHalfAnswer(t *testing.T) {
	a := halfAnswer()
	if a != 42/2 {
		t.Errorf("answer: got %d wanted %d\n", a, 42/2)
	}
}

func TestWhichAnswer(t *testing.T) {
	cases := []struct {
		has  bool
		want int
	}{
		// {true, 21},
		{false, 42},
	}

	for _, c := range cases {
		if a := whichAnswer(c.has); a != c.want {
			t.Errorf("answer: got %d wanted %d\n", a, c.want)
		}
	}
}
