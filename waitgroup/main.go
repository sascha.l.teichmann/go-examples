package main

import (
	"fmt"
	"sync"
)

func counter(base, incr, max int, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := base; i <= max; i += incr {
		fmt.Println(i)
	}
}

func main() {
	var wg sync.WaitGroup

	for _, i := range []int{2, 3, 5, 7, 11, 13, 17, 19} {
		wg.Add(1)
		go counter(i, i, 20, &wg)
	}
	wg.Wait()
}
