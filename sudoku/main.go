package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

/*
 0  1  2  3  4  5  6  7  8
 9 10 11 12 13 14 15 16 17
18 19 20 21 22 23 24 25 26
27 28 29 30 31 32 33 34 35
36 37 38 39 40 41 42 43 44
45 46 47 48 49 50 51 52 53
54 55 56 57 58 59 60 61 62
63 64 65 66 67 68 69 70 71
72 73 74 75 76 77 78 79 80
*/

var checks = func() [][][]byte {
	indices := make([]byte, 3*81)

	lookup := make([][][]byte, 81)
	for i := range lookup {
		lookup[i] = make([][]byte, 0, 3)
	}

	register := func(checks []byte) {
		for ; len(checks) > 0; checks = checks[9:] {
			check := checks[:9]
			for _, idx := range check {
				lookup[idx] = append(lookup[idx], check)
			}
		}
	}

	a := indices[:81]
	for i := range a {
		a[i] = byte(i)
	}
	register(a)

	b := indices[81 : 2*81]
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			b[i*9+j] = byte(j*9 + i)
		}
	}
	register(b)

	c := indices[2*81:]
	for m, p := 0, 0; m < 3; m++ {
		for n := 0; n < 3; n++ {
			x, y := m*3, n*3
			for i := 0; i < 3; i++ {
				for j := 0; j < 3; j, p = j+1, p+1 {
					c[p] = byte((i+x)*9 + y + j)
				}
			}
		}
	}
	register(c)

	return lookup
}()

type board [9 * 9]byte

func (b *board) String() string {
	var buf bytes.Buffer
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if v := b[i*9+j]; v != 0 {
				fmt.Fprintf(&buf, "%d", v)
			} else {
				buf.WriteRune('.')
			}
		}
		buf.WriteByte('\n')
	}
	return buf.String()
}

func (b *board) read(in io.Reader) error {
	scanner := bufio.NewScanner(in)
	n := 0
	for n < 9 && scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(line, "#") {
			continue
		}
		m := n * 9
		for _, c := range line {
			if c >= '1' && c <= '9' {
				b[m] = byte(c - '0')
			}
			m++
		}
		n++
	}
	return scanner.Err()
}

func (b *board) check(idx int) bool {
	for _, ch := range checks[idx] {
		mask := 0
		for _, i := range ch {
			if v := b[i]; v != 0 {
				m := 1 << v
				if mask&m != 0 {
					return false
				}
				mask |= m
			}
		}
	}
	return true
}

func (b *board) solved() bool {
	for i := len(b) - 1; i >= 0; i-- {
		if b[i] == 0 {
			return false
		}
	}
	return true
}

type solver []*board

func (s *solver) solve(b *board, idx int) {
	if b.solved() {
		*s = append(*s, b)
		return
	}
	for ; idx < len(b); idx++ {
		if b[idx] != 0 {
			continue
		}
		for w := byte(1); w <= 9; w++ {
			b[idx] = w
			if b.check(idx) {
				c := *b
				s.solve(&c, idx+1)
			}
		}
		break
	}
}

func main() {
	var b board
	if err := b.read(os.Stdin); err != nil {
		log.Fatalln(err)
	}
	var s solver
	s.solve(&b, 0)
	fmt.Printf("# number solutions: %d\n", len(s))
	for i, b := range s {
		if i > 0 {
			fmt.Println()
		}
		fmt.Print(b)
	}
}
