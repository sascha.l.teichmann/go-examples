package main

import (
	"fmt"
	"time"
)

// answers produces the relevant.
func answers(out chan<- int, done <-chan struct{}) {
	for {
		select {
		case out <- 42:
		case <-done:
			fmt.Println("no more answers")
			return
		}
	}
}

func run() {
	ch := make(chan int)
	done := make(chan struct{})
	defer func() {
		done <- struct{}{}
	}()
	go answers(ch, done)

	for answer := range ch {
		if answer == 42 {
			fmt.Println("Answer found!")
			return
		}
	}
}

func main() {
	run()
	time.Sleep(1 * time.Second)
}
