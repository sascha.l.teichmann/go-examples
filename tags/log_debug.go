// +build debug

package main

import lg "log"

const debug = true

func log(msg string) {
	lg.Println(msg)
}
