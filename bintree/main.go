package main

import (
	"cmp"
	"flag"
	"fmt"
	"iter"
	"math/rand"
)

type (
	tree[E any] struct {
		root *node[E]
		cmp  func(a, b E) int
	}

	node[E any] struct {
		left, right *node[E]
		data        E
	}
)

func newTree[E any](cmp func(a, b E) int) *tree[E] {
	return &tree[E]{cmp: cmp}
}

func (t *tree[E]) insert(e E) {
	var recurse func(*node[E]) *node[E]
	recurse = func(n *node[E]) *node[E] {
		if n == nil {
			return &node[E]{data: e}
		}
		if t.cmp(e, n.data) < 0 {
			n.left = recurse(n.left)
		} else {
			n.right = recurse(n.right)
		}
		return n
	}
	t.root = recurse(t.root)
}

func (t *tree[E]) inorder() iter.Seq[E] {
	return func(yield func(E) bool) {
		var recurse func(n *node[E]) bool
		recurse = func(n *node[E]) bool {
			return n == nil ||
				recurse(n.left) &&
					yield(n.data) &&
					recurse(n.right)
		}
		recurse(t.root)
	}
}

type item struct {
	number int
	price  float64
}

func (i *item) byPrice(o *item) int {
	return cmp.Compare(i.price, o.price)
}

func (i *item) byNumber(o *item) int {
	return cmp.Compare(i.number, o.number)
}

func reverse[E any](cmp func(a, b E) int) func(a, b E) int {
	return func(a, b E) int {
		return cmp(b, a)
	}
}

func main() {

	price := flag.Bool("price", false, "order by price")
	rev := flag.Bool("reverse", false, "reverse order")
	flag.Parse()

	cmp := (*item).byNumber
	if *price {
		cmp = (*item).byPrice
	}
	if *rev {
		cmp = reverse(cmp)
	}

	t := newTree(cmp)

	for range 20 {
		t.insert(&item{
			number: rand.Intn(100),
			price:  float64(rand.Intn(10_000)) / 100,
		})
	}

	for v := range t.inorder() {
		fmt.Printf("n: %2d p: %.2f\n", v.number, v.price)
	}
}
