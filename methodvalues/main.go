package main

import "fmt"

type greeter string

func (g greeter) greet(who string) {
	fmt.Printf("%s, %s!\n", g, who)
}

func do(h func(string)) {
	h("Doer")
}

func main() {

	g := greeter("Hello")

	g.greet("World")

	f := g.greet

	f("Moon")

	do(f)
}
