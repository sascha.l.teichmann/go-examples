package main

import "fmt"

func hello(s string) {
	fmt.Printf("Hello, %s!\n", s)
}

func main() {
	hello("World")
}
