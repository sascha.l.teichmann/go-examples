package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

type coord struct {
	X int
	Y int
}

type document struct {
	Name     string `json:"name"`
	Location *coord `json:"location"`
}

func (c *coord) UnmarshalJSON(data []byte) error {
	var slice []int
	if err := json.Unmarshal(data, &slice); err != nil {
		return err
	}
	if len(slice) != 2 {
		return fmt.Errorf(
			"Slice too short: length %d want 2.", len(slice))
	}

	*c = coord{slice[0], slice[1]}

	return nil
}

func (c *coord) MarshalJSON() ([]byte, error) {
	if c == nil {
		return nil, nil
	}
	return json.Marshal([]int{c.X, c.Y})
}

func load(filename string) (*document, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	doc := new(document)
	dec := json.NewDecoder(bufio.NewReader(f))
	if err = dec.Decode(doc); err != nil {
		return nil, err
	}
	return doc, nil
}

func (doc *document) save(filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	out := bufio.NewWriter(f)
	enc := json.NewEncoder(out)
	if err = enc.Encode(doc); err != nil {
		f.Close()
		return err
	}
	if err = out.Flush(); err != nil {
		f.Close()
		return err
	}
	return f.Close()
}

func main() {

	flag.Parse()

	if flag.NArg() != 2 {
		log.Fatalln("missing argument")
	}

	doc, err := load(flag.Arg(0))
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("in: %#v\n", doc)

	doc.Name += " (modified)"

	if doc.Location != nil {
		doc.Location.X += 10
		doc.Location.Y += 10
	}

	if err = doc.save(flag.Arg(1)); err != nil {
		log.Fatalln(err)
	}
}
