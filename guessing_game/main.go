package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
)

func main() {
	secret := rand.Intn(100) + 1
	fmt.Printf("The secret number is: %d\n", secret)

	input := bufio.NewScanner(os.Stdin)
loop:
	for {
		fmt.Println("Please input your guess.")
		if !input.Scan() {
			break
		}
		guess, err := strconv.Atoi(input.Text())
		if err != nil {
			continue
		}
		switch {
		case guess < secret:
			fmt.Println("Too small!")
		case guess > secret:
			fmt.Println("Too big!")
		default:
			fmt.Println("You win!")
			break loop
		}
	}
	if err := input.Err(); err != nil {
		log.Fatalln(err)
	}
}
