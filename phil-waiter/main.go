package main

import (
	"fmt"
	"sync"
)

var names = [...]string{
	"Marx",
	"Aristoteles",
	"Spinoza",
	"Kant",
	"Russel",
}

const meals = 30

type need struct {
	forks []int
	ready chan struct{}
}

type waiter struct {
	forks    []bool
	waiting  []*need
	incoming chan *need
	releases chan int
}

func newWaiter(n int) *waiter {
	return &waiter{
		forks:    make([]bool, n),
		incoming: make(chan *need),
		releases: make(chan int),
	}
}

func (w *waiter) acquire(ready chan struct{}, forks ...int) {
	w.incoming <- &need{
		forks: forks,
		ready: ready,
	}
	<-ready
}

func (w *waiter) release(forks ...int) {
	for _, fork := range forks {
		w.releases <- fork
	}
}

func (w *waiter) grant(n *need) bool {
	for _, fork := range n.forks {
		if w.forks[fork] {
			return false
		}
	}
	for _, fork := range n.forks {
		w.forks[fork] = true
	}
	n.ready <- struct{}{}
	return true
}

func (w *waiter) serve(done <-chan chan<- struct{}) {
	for {
		select {
		case ch := <-done:
			ch <- struct{}{}
			return
		case n := <-w.incoming:
			if !w.grant(n) {
				w.waiting = append(w.waiting, n)
			}
		case fork := <-w.releases:
			w.forks[fork] = false
			for i, n := range w.waiting {
				if w.grant(n) {
					copy(w.waiting[i:], w.waiting[i+1:])
					w.waiting[len(w.waiting)-1] = nil
					w.waiting = w.waiting[:len(w.waiting)-1]
					break
				}
			}
		}
	}
}

func dine(name string, left, right int, w *waiter, wg *sync.WaitGroup) {
	defer wg.Done()
	ready := make(chan struct{})
	for i := 0; i < meals; i++ {
		w.acquire(ready, left, right)
		fmt.Printf("%s eats\n", name)
		w.release(left, right)
	}
}

func main() {

	var wg sync.WaitGroup

	w := newWaiter(len(names))

	for i, name := range names {
		wg.Add(1)
		go dine(name, i, (i+1)%len(names), w, &wg)
	}

	done := make(chan chan<- struct{})
	go w.serve(done)

	wg.Wait()

	ch := make(chan struct{})
	done <- ch
	<-ch
}
