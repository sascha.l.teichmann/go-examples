package main

import (
	"fmt"
	"reflect"
)

func main() {

	var x float64 = 1.7
	fmt.Println("x type:", reflect.TypeOf(x))

	v := reflect.ValueOf(x)
	fmt.Println("v value:", v)
	fmt.Println("v kind:", v.Kind())
	fmt.Println("v type:", v.Type())
	fmt.Println("v can set:", v.CanSet())
	fmt.Printf("v interface: %v\n", v.Interface())
	fmt.Println()

	type F float64

	f := F(3.1415)
	vf := reflect.ValueOf(f)
	vt := reflect.TypeOf(f)
	fmt.Println("vf type:", vt)
	fmt.Println("vf kind:", vf.Kind())
	fmt.Println("vf value type:", vf.Type())
	fmt.Println()

	xp := &x

	vxp := reflect.ValueOf(xp)
	vxt := reflect.TypeOf(xp)
	fmt.Println("xp type:", vxt)
	fmt.Println("xp value type:", vxt)
	fmt.Println("xp can set:", vxp.CanSet())
	e := vxp.Elem()
	fmt.Println("xpe can set:", e.CanSet())
	fmt.Println("deref pointer before:", *xp)
	e.SetFloat(e.Float() + 1.0)
	fmt.Println("deref pointer after:", *xp)
}
