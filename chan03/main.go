package main

import "fmt"

func main() {

	c := make(chan int, 1)
	c <- 42

	if v, ok := <-c; !ok {
		fmt.Println("channel is already closed")
	} else {
		fmt.Printf("value is %d\n", v)
	}

	close(c)

	if _, ok := <-c; !ok {
		fmt.Println("channel is already closed")
	}
}
