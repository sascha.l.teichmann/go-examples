package main

import "sync"

func main() {
	var mu sync.Mutex
	var problem int
	for i := 0; i < 5; i++ {
		go func() {
			for j := 0; j < 10; j++ {
				mu.Lock()
				problem++
				mu.Unlock()
			}
		}()
	}

}
