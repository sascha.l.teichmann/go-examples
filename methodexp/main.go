package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
)

type stackError error

var errEmpty = stackError(errors.New("Stack empty"))

type machine struct {
	stack []float64
}

func (m *machine) push(x float64) {
	m.stack = append(m.stack, x)
}

func (m *machine) pop() float64 {
	l := len(m.stack) - 1
	if l < 0 {
		panic(errEmpty)
	}
	x := m.stack[l]
	m.stack = m.stack[:l]
	return x
}

func (m *machine) top() float64 {
	l := len(m.stack) - 1
	if l < 0 {
		panic(errEmpty)
	}
	return m.stack[l]
}

func (m *machine) operate(f func(a, b float64) float64) {
	b := m.pop()
	a := m.pop()
	m.push(f(a, b))
}

func (m *machine) add(string) {
	m.operate(func(a, b float64) float64 {
		return a + b
	})
}

func (m *machine) sub(string) {
	m.operate(func(a, b float64) float64 {
		return a - b
	})
}

func (m *machine) mul(string) {
	m.operate(func(a, b float64) float64 {
		return a * b
	})
}

func (m *machine) div(string) {
	m.operate(func(a, b float64) float64 {
		return a / b
	})
}

func (m *machine) dup(string) {
	x := m.top()
	m.push(x)
	m.push(x)
}

func (m *machine) print(string) {
	fmt.Printf("%f\n", m.top())
}

func (m *machine) out(string) {
	fmt.Printf("%f\n", m.pop())
}

func (m *machine) drop(string) {
	m.pop()
}

func (m *machine) swap(string) {
	a := m.pop()
	b := m.pop()
	m.push(a)
	m.push(b)
}

func (m *machine) number(s string) {
	x, err := strconv.ParseFloat(s, 64)
	if err != nil {
		log.Printf("not a number: %v\n", err)
		return
	}
	m.push(x)
}

var ops = map[string]func(*machine, string){
	"+":    (*machine).add,
	"-":    (*machine).sub,
	"*":    (*machine).mul,
	"/":    (*machine).div,
	"dup":  (*machine).dup,
	"drop": (*machine).drop,
	"swap": (*machine).swap,
	"?":    (*machine).print,
	".":    (*machine).out,
}

func (m *machine) execute(s string) (err error) {
	defer func() {
		if p := recover(); p != nil {
			if serr, ok := p.(stackError); ok {
				err = serr
			} else {
				panic(p)
			}
		}
	}()

	op := ops[s]
	if op == nil {
		op = (*machine).number
	}
	op(m, s)

	return
}

func main() {

	scan := bufio.NewScanner(os.Stdin)

	m := new(machine)

	for scan.Scan() {
		line := scan.Text()
		if err := m.execute(line); err != nil {
			log.Printf("execute error: %v\n", err)
		}
	}

	if err := scan.Err(); err != nil {
		log.Printf("scan error: %v\n", err)
	}
}
